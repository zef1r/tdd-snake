import pytest

from mockify.actions import Return
from mockify.matchers import _

from snake.game import Game, Stats
from snake.utils import Vec2
from snake.ports.stubs import Screen


class TestBoard:

    @pytest.fixture(autouse=True)
    def setup(self, randomizer_mock, status_listener_mock):
        self.randomizer = randomizer_mock
        self.status_listener = status_listener_mock

    def make_game(self, size):
        screen = Screen(size)
        game = Game(screen, self.randomizer, self.status_listener, screen.size)
        return game, screen

    def make_and_start_game(self, size, food_pos):
        self.status_listener.update_stats.\
            expect_call(Stats(0, 1))
        self.randomizer.randomize_food.\
            expect_call(_).will_once(Return(food_pos))

        game, screen = self.make_game(size)
        game.start()
        return game, screen

    # Tests

    def test_when_game_starts__then_display_food_and_randomly_picked_position_and_snake_in_the_center(self):
        self.status_listener.update_stats.\
            expect_call(Stats(0, 1))
        self.randomizer.randomize_food.\
            expect_call({Vec2(0, 0), Vec2(1, 0), Vec2(3, 0), Vec2(4, 0)}).\
            will_once(Return(Vec2(0, 0)))

        game, screen = self.make_game(Vec2(5, 1))
        game.start()
        assert game.food_pos == Vec2(0, 0)
        assert game.snake_head_pos == Vec2(2, 0)

        assert screen.rows == [('@', ' ', '+', ' ', ' ')]

    def test_move_snake_to_the_left(self):
        game, screen = self.make_and_start_game(Vec2(6, 1), Vec2(0, 0))
        assert screen.rows == [('@', ' ', ' ', '+', ' ', ' ')]
        game.move_left()
        game.update()
        assert screen.rows == [('@', ' ', '+', ' ', ' ', ' ')]
        game.update()
        assert screen.rows == [('@', '+', ' ', ' ', ' ', ' ')]

    def test_move_snake_to_the_right(self):
        game, screen = self.make_and_start_game(Vec2(6, 1), Vec2(0, 0))
        assert screen.rows == [('@', ' ', ' ', '+', ' ', ' ')]
        game.move_right()
        game.update()
        assert screen.rows == [('@', ' ', ' ', ' ', '+', ' ')]
        game.update()
        assert screen.rows == [('@', ' ', ' ', ' ', ' ', '+')]

    def test_move_snake_up(self):
        game, screen = self.make_and_start_game(Vec2(1, 6), Vec2(0, 0))
        assert screen.rows == [('@',), (' ',), (' ',), ('+',), (' ',), (' ',)]
        game.move_up()
        game.update()
        assert screen.rows == [('@',), (' ',), ('+',), (' ',), (' ',), (' ',)]
        game.update()
        assert screen.rows == [('@',), ('+',), (' ',), (' ',), (' ',), (' ',)]

    def test_move_snake_down(self):
        game, screen = self.make_and_start_game(Vec2(1, 6), Vec2(0, 0))
        assert screen.rows == [('@',), (' ',), (' ',), ('+',), (' ',), (' ',)]
        game.move_down()
        game.update()
        assert screen.rows == [('@',), (' ',), (' ',), (' ',), ('+',), (' ',)]
        game.update()
        assert screen.rows == [('@',), (' ',), (' ',), (' ',), (' ',), ('+',)]

    def test_when_snake_head_collides_with_left_wall__then_game_is_over(self):
        game, screen = self.make_and_start_game(Vec2(3, 1), Vec2(2, 0))
        game.move_left()
        game.update()

        self.status_listener.game_over.\
            expect_call('left_wall_collision')

        game.update()

    def test_when_snake_head_collides_with_right_wall__then_game_is_over(self):
        game, screen = self.make_and_start_game(Vec2(3, 1), Vec2(0, 0))
        game.move_right()
        game.update()

        self.status_listener.game_over.\
            expect_call('right_wall_collision')

        game.update()

    def test_when_snake_head_collides_with_top_wall__then_game_is_over(self):
        game, screen = self.make_and_start_game(Vec2(1, 3), Vec2(0, 2))
        game.move_up()
        game.update()

        self.status_listener.game_over.\
            expect_call('top_wall_collision')

        game.update()

    def test_when_snake_head_collides_with_bottom_wall__then_game_is_over(self):
        game, screen = self.make_and_start_game(Vec2(1, 3), Vec2(0, 0))
        game.move_down()
        game.update()

        self.status_listener.game_over.\
            expect_call('bottom_wall_collision')

        game.update()

    def test_when_game_is_over__then_nothing_changes_on_update(self):
        game, screen = self.make_and_start_game(Vec2(1, 3), Vec2(0, 0))
        game.move_down()
        game.update()

        self.status_listener.game_over.\
            expect_call('bottom_wall_collision')

        game.update()

        game.move_up()
        game.update()
        game.update()

    def test_when_snake_eats_food__it_grows_by_one_and_signals_status_update(self):
        self.status_listener.update_stats.\
            expect_call(Stats(0, 1))
        self.randomizer.randomize_food.expect_call(_).\
            will_once(Return(Vec2(0, 0))).\
            will_once(Return(Vec2(2, 0)))

        game, screen = self.make_game(Vec2(3, 1))
        game.start()
        assert screen.rows == [('@', '+', ' ')]
        game.move_left()

        self.status_listener.update_stats.\
            expect_call(Stats(food=1, length=2))

        game.update()
        assert screen.rows == [('+', '*', '@')]

    def test_when_snake_collides_with_itself__then_game_is_over(self):
        self.randomizer.randomize_food.expect_call(_).\
            will_once(Return(Vec2(0, 0))).\
            will_once(Return(Vec2(1, 0))).\
            will_once(Return(Vec2(2, 0))).\
            will_once(Return(Vec2(2, 1))).\
            will_once(Return(Vec2(2, 2)))

        self.status_listener.update_stats.expect_call(Stats(0, 1))
        self.status_listener.update_stats.expect_call(Stats(food=1, length=2))
        self.status_listener.update_stats.expect_call(Stats(food=2, length=3))
        self.status_listener.update_stats.expect_call(Stats(food=3, length=4))
        self.status_listener.update_stats.expect_call(Stats(food=4, length=5))

        game, screen = self.make_game(Vec2(3, 3))
        game.start()
        assert screen.rows == [
            ('@', ' ', ' '),
            (' ', '+', ' '),
            (' ', ' ', ' '),
        ]

        game.move_left()
        game.update()
        game.move_up()
        game.update()
        assert screen.rows == [
            ('+', '@', ' '),
            ('*', ' ', ' '),
            (' ', ' ', ' '),
        ]

        game.move_right()
        game.update()
        assert screen.rows == [
            ('*', '+', '@'),
            ('*', ' ', ' '),
            (' ', ' ', ' '),
        ]

        game.update()
        assert screen.rows == [
            ('*', '*', '+'),
            ('*', ' ', '@'),
            (' ', ' ', ' '),
        ]

        game.move_down()
        game.update()
        assert screen.rows == [
            ('*', '*', '*'),
            ('*', ' ', '+'),
            (' ', ' ', '@'),
        ]

        game.move_left()
        game.update()
        assert screen.rows == [
            ('*', '*', '*'),
            (' ', '+', '*'),
            (' ', ' ', '@'),
        ]

        game.move_up()

        self.status_listener.game_over.\
            expect_call('self_collision')

        game.update()

    def test_when_snake_is_longer_that_one_and_is_moving_to_the_left__attempt_to_move_it_right_is_ignored(self):
        self.status_listener.update_stats.expect_call(_).times(2)
        self.randomizer.randomize_food.expect_call(_).\
            will_once(Return(Vec2(1, 0))).\
            will_once(Return(Vec2(4, 0)))

        game, screen = self.make_game(Vec2(5, 1))
        game.start()
        assert screen.rows == [(' ', '@', '+', ' ', ' ')]

        game.move_left()
        game.update()
        assert screen.rows == [(' ', '+', '*', ' ', '@')]

        game.move_right()
        game.update()
        assert screen.rows == [('+', '*', ' ', ' ', '@')]

    def test_when_snake_is_longer_that_one_and_is_moving_to_the_right__attempt_to_move_it_left_is_ignored(self):
        self.status_listener.update_stats.expect_call(_).times(2)
        self.randomizer.randomize_food.expect_call(_).\
            will_once(Return(Vec2(3, 0))).\
            will_once(Return(Vec2(0, 0)))

        game, screen = self.make_game(Vec2(5, 1))
        game.start()
        assert screen.rows == [(' ', ' ', '+', '@', ' ')]

        game.move_right()
        game.update()
        assert screen.rows == [('@', ' ', '*', '+', ' ')]

        game.move_left()
        game.update()
        assert screen.rows == [('@', ' ', ' ', '*', '+')]

    def test_when_snake_is_longer_that_one_and_is_moving_up__attempt_to_move_it_down_is_ignored(self):
        self.status_listener.update_stats.expect_call(_).times(2)
        self.randomizer.randomize_food.expect_call(_).\
            will_once(Return(Vec2(0, 1))).\
            will_once(Return(Vec2(0, 4)))

        game, screen = self.make_game(Vec2(1, 5))
        game.start()
        assert screen.rows == [(' ',), ('@',), ('+',), (' ',), (' ',)]

        game.move_up()
        game.update()
        assert screen.rows == [(' ',), ('+',), ('*',), (' ',), ('@',)]

        game.move_down()
        game.update()
        assert screen.rows == [('+',), ('*',), (' ',), (' ',), ('@',)]

    def test_when_snake_is_longer_that_one_and_is_moving_down__attempt_to_move_it_up_is_ignored(self):
        self.status_listener.update_stats.expect_call(_).times(2)
        self.randomizer.randomize_food.expect_call(_).\
            will_once(Return(Vec2(0, 3))).\
            will_once(Return(Vec2(0, 0)))

        game, screen = self.make_game(Vec2(1, 5))
        game.start()
        assert screen.rows == [(' ',), (' ',), ('+',), ('@',), (' ',)]

        game.move_down()
        game.update()
        assert screen.rows == [('@',), (' ',), ('*',), ('+',), (' ',)]

        game.move_up()
        game.update()
        assert screen.rows == [('@',), (' ',), (' ',), ('*',), ('+',)]
