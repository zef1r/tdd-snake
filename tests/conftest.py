import pytest

from mockify import Registry
from mockify.mock import Object


@pytest.fixture
def mock_registry():
    registry = Registry()
    yield registry
    registry.assert_satisfied()


@pytest.fixture
def randomizer_mock(mock_registry):
    methods = ['randomize_food', 'randomize_snake', 'randomize_direction']
    randomizer = Object('randomizer', methods=methods, registry=mock_registry)
    return randomizer


@pytest.fixture
def screen_mock(mock_registry):
    methods = ['draw_snake', 'draw_food']
    screen = Object('screen', methods=methods, registry=mock_registry)
    return screen


@pytest.fixture
def status_listener_mock(mock_registry):
    methods = ['update_stats', 'game_over']
    status_listener = Object('status_listener', methods=methods, registry=mock_registry)
    return status_listener
