#!/usr/bin/env python

from snake.main import SnakeApplication


if __name__ == '__main__':
    app = SnakeApplication()
    app.run()
