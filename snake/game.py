import itertools
import collections

from .utils import Vec2


Stats = collections.namedtuple('Stats', 'food, length')


class Game:

    def __init__(self, screen, randomizer, status_listener, board_size):
        self._screen = screen
        self._randomizer = randomizer
        self._status_listener = status_listener
        self._board_size = board_size
        self._snake = collections.deque([board_size // 2])
        self._direction = None
        self._food_eaten = 0
        self._game_ended = False

    def start(self):
        self._insert_food()
        self._screen.draw_snake_head(self._snake[0])
        self._status_listener.update_stats(self.stats)

    def move_left(self):
        self._move(Vec2(-1, 0))

    def move_right(self):
        self._move(Vec2(1, 0))

    def move_up(self):
        self._move(Vec2(0, -1))

    def move_down(self):
        self._move(Vec2(0, 1))

    def _move(self, direction):
        if len(self._snake) < 2 or self._snake[0] + direction != self._snake[1]:
            self._direction = direction

    def update(self):
        if self._game_ended:
            return
        prev_head = self._snake[0]
        tail = self._snake[-1]
        head = prev_head + self._direction
        if head == self._food_pos:
            self._food_eaten += 1
            self._snake.appendleft(head)
            self._status_listener.update_stats(self.stats)
            self._screen.draw_snake_head(head)
            self._screen.draw_snake_body(prev_head)
            self._insert_food()
        elif head.x < 0:
            self._signal_game_over('left_wall_collision')
        elif head.x >= self._board_size.x:
            self._signal_game_over('right_wall_collision')
        elif head.y < 0:
            self._signal_game_over('top_wall_collision')
        elif head.y >= self._board_size.y:
            self._signal_game_over('bottom_wall_collision')
        elif head in self._snake:
            self._signal_game_over('self_collision')
        else:
            self._snake.appendleft(head)
            self._snake.pop()
            self._screen.draw_snake_head(head)
            if prev_head != tail:
                self._screen.draw_snake_body(prev_head)
            self._screen.clear(tail)

    def _signal_game_over(self, reason):
        self._status_listener.game_over(reason)
        self._game_ended = True

    def _insert_food(self):
        self._food_pos = self._randomizer.randomize_food(self.free_cells)
        self._screen.draw_food(self._food_pos)

    @property
    def free_cells(self):
        w, h = self._board_size
        all_cells = {Vec2(*x) for x in itertools.product(range(w), range(h))}
        return all_cells - set(self._snake)

    @property
    def food_pos(self):
        return self._food_pos

    @property
    def snake_head_pos(self):
        return self._snake[0]

    @property
    def stats(self):
        return Stats(self._food_eaten, len(self._snake))
