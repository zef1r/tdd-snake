class Vec2:
    __slots__ = ('x', 'y')

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"Vec2({self.x!r}, {self.y!r})"

    def __hash__(self):
        return hash((self.x, self.y))

    def __getitem__(self, key):
        if key == 0:
            return self.x
        elif key == 1:
            return self.y
        else:
            raise IndexError(key)

    def __iter__(self):
        yield self.x
        yield self.y

    def __eq__(self, other):
        return isinstance(other, self.__class__) and\
            self.x == other.x and\
            self.y == other.y

    def __floordiv__(self, other):
        return Vec2(self.x // other, self.y // other)

    def __add__(self, other):
        return Vec2(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vec2(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        return Vec2(self.x * other, self.y * other)
