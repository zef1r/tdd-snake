from snake.game import Game
from snake.utils import Vec2
from snake.events import PlayerAction
from snake.ports.pygame import EventLoop, Screen, Viewport
from snake.ports.randomizer import Randomizer

_BOARD_SIZE = Vec2(25, 25)


class SnakeApplication:

    def run(self):
        _PygameApplication().run()


class _PygameApplication:

    def __init__(self):
        self._event_loop = EventLoop()
        self._screen = Screen(_BOARD_SIZE)
        self._viewport = Viewport(self._screen)
        self._randomizer = Randomizer()
        self._game = Game(self._viewport, self._randomizer, self._viewport, _BOARD_SIZE)

    def run(self):
        self._game.move_left()
        self._game.start()
        self._event_loop.listen(PlayerAction, self._on_player_action)
        self._event_loop.call_repeatedly(self._on_tick, interval=0.150)
        self._event_loop.run()

    def _on_player_action(self, event):
        if event.action == PlayerAction.Move.LEFT:
            self._game.move_left()
        elif event.action == PlayerAction.Move.RIGHT:
            self._game.move_right()
        elif event.action == PlayerAction.Move.UP:
            self._game.move_up()
        elif event.action == PlayerAction.Move.DOWN:
            self._game.move_down()

    def _on_tick(self):
        self._game.update()
        self._screen.flip()
