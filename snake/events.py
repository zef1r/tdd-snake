import enum


class Event:

    def __init__(self, **params):
        for k, v in params.items():
            setattr(self, k, v)

    def __repr__(self):
        tmp = [f"{k}={v!r}" for k, v in sorted(self.__dict__.items())]
        tmp = ', '.join(tmp)
        return f"{self.__class__.__name__}({tmp})"

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class PlayerAction(Event):

    class Move(enum.Enum):
        LEFT = 0
        RIGHT = 1
        UP = 2
        DOWN = 3

    def __init__(self, action):
        super().__init__(action=action)
