import random


class Randomizer:

    def randomize_food(self, free_cells):
        return random.choice(list(free_cells))
