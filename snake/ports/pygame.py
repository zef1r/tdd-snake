import time
import collections

import pygame

from snake.events import PlayerAction

_GRID_CELL_SIZE = (25, 25) # Size of single grid cell in pixels (width and height)
_GRID_COLOR = (16, 16, 16) # Grid color (RGB)
_FOOD_COLOR = (128, 0, 0)
_SNAKE_HEAD_COLOR = (0, 192, 0)
_SNAKE_BODY_COLOR = (0, 128, 0)


class EventLoop:

    class _RepeatedFunction:

        def __init__(self, func, interval=None):
            self._func = func
            self._interval = interval
            self._last_call_time = None

        def __call__(self):
            if self._interval is None:
                self._func()
            elif self._last_call_time is None:
                self._last_call_time = time.clock()
                self._func()
            elif time.clock() - self._last_call_time > self._interval:
                self._last_call_time = time.clock()
                self._func()

    def __init__(self):
        pygame.init()
        pygame.font.init()
        self._repeated_callbacks = []
        self._event_listeners = {}

    def listen(self, event_type, listener):
        self._event_listeners.setdefault(event_type, []).append(listener)

    def call_repeatedly(self, func, interval=None):
        self._repeated_callbacks.append(
            self._RepeatedFunction(func, interval))

    def run(self, fps=60):
        clock = pygame.time.Clock()
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self._notify(PlayerAction, PlayerAction.Move.LEFT)
                    elif event.key == pygame.K_RIGHT:
                        self._notify(PlayerAction, PlayerAction.Move.RIGHT)
                    elif event.key == pygame.K_UP:
                        self._notify(PlayerAction, PlayerAction.Move.UP)
                    elif event.key == pygame.K_DOWN:
                        self._notify(PlayerAction, PlayerAction.Move.DOWN)
            for callback in self._repeated_callbacks:
                callback()
            clock.tick(fps)

    def _notify(self, event_type, *args, **kwargs):
        event = event_type(*args, **kwargs)
        for listener in self._event_listeners.get(event_type, []):
            listener(event)


class Screen:

    def __init__(self, board_size):
        self._board_size = board_size
        self._width = int((self._board_size.x * _GRID_CELL_SIZE[0]) * 1.5 + _GRID_CELL_SIZE[0])
        self._height = int((self._board_size.y * _GRID_CELL_SIZE[1]) + 2 * _GRID_CELL_SIZE[1])
        self._screen = pygame.display.set_mode((self._width, self._height))

    def flip(self):
        pygame.display.flip()

    @property
    def board_size(self):
        return self._board_size

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height


class Viewport:

    def __init__(self, screen):
        self._screen = screen
        self.__draw_grid()

    def __draw_grid(self):
        screen = self._screen._screen
        width = self._screen.board_size.x * _GRID_CELL_SIZE[0]
        height = self._screen.board_size.y * _GRID_CELL_SIZE[1]
        for y in range(_GRID_CELL_SIZE[1], height + _GRID_CELL_SIZE[1] + 1, _GRID_CELL_SIZE[1]):
            pygame.draw.line(screen, _GRID_COLOR, (_GRID_CELL_SIZE[0], y), (_GRID_CELL_SIZE[0] + width - 1, y))
        for x in range(_GRID_CELL_SIZE[0], width + _GRID_CELL_SIZE[0] + 1, _GRID_CELL_SIZE[0]):
            pygame.draw.line(screen, _GRID_COLOR, (x, _GRID_CELL_SIZE[1]), (x, _GRID_CELL_SIZE[1] + height - 1))

    def update_stats(self, stats):
        font = pygame.font.SysFont('serif', 30)
        label = font.render(f'Score: {stats.food}', True, (128, 128, 128))
        pygame.draw.rect(self._screen._screen, (0, 0, 0),
            pygame.Rect(self._screen.width - 300, _GRID_CELL_SIZE[1], 200, 100))
        self._screen._screen.blit(label, (self._screen.width - 300, _GRID_CELL_SIZE[1]))

    def game_over(self, reason):
        font = pygame.font.SysFont('serif', 50)
        label = font.render("GAME OVER", True, (192, 0, 0))
        self._screen._screen.blit(label, (self._screen.width // 2 - 300, self._screen.height // 2 - 25))

    def draw_food(self, pos):
        r = 0.3 * min(_GRID_CELL_SIZE[0], _GRID_CELL_SIZE[1])
        x = _GRID_CELL_SIZE[0] + pos.x * _GRID_CELL_SIZE[0] + 0.5 * _GRID_CELL_SIZE[0]
        y = _GRID_CELL_SIZE[1] + pos.y * _GRID_CELL_SIZE[1] + 0.5 * _GRID_CELL_SIZE[1]
        pygame.draw.circle(self._screen._screen, _FOOD_COLOR, (int(x), int(y)), int(r))

    def draw_snake_head(self, pos):
        self._draw_segment(pos, _SNAKE_HEAD_COLOR)

    def draw_snake_body(self, pos):
        self._draw_segment(pos, _SNAKE_BODY_COLOR)

    def clear(self, pos):
        self._draw_segment(pos, (0, 0, 0))

    def _draw_segment(self, pos, color):
        x = _GRID_CELL_SIZE[0] + pos[0] * _GRID_CELL_SIZE[0]
        y = _GRID_CELL_SIZE[1] + pos[1] * _GRID_CELL_SIZE[1]
        pygame.draw.rect(self._screen._screen, color,
            pygame.Rect(x+1, y+1, _GRID_CELL_SIZE[0]-2, _GRID_CELL_SIZE[1]-2))
