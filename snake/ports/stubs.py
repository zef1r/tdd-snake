import collections


class Screen:

    def __init__(self, size):
        self._size = size
        self._rows = [
            [' ' for _ in range(size.x)] for _ in range(size.y)
        ]

    def draw_food(self, pos):
        self._rows[pos.y][pos.x] = '@'

    def draw_snake_head(self, pos):
        self._rows[pos.y][pos.x] = '+'

    def draw_snake_body(self, pos):
        self._rows[pos.y][pos.x] = '*'

    def clear(self, pos):
        self._rows[pos.y][pos.x] = ' '

    @property
    def size(self):
        return self._size

    @property
    def rows(self):
        return [
            tuple(row) for row in self._rows
        ]
